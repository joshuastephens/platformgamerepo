﻿using UnityEngine;
using System.Collections;

public class CheckPoint : MonoBehaviour {
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            other.gameObject.GetComponent<PlayerController25d>().CheckPoint = other.gameObject.transform.position;
        }
    }
}
