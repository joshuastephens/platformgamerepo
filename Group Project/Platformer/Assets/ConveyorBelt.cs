﻿using UnityEngine;
using System.Collections;

public class ConveyorBelt : MonoBehaviour
{
    public float speed = 10000;
    public float speed2=.5f;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }


    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player" && other.attachedRigidbody)
        {
            other.gameObject.GetComponent<Rigidbody>().AddForce(transform.rotation * Vector3.right * -speed);
        }
        if(other.gameObject.tag == "Crate")
        {
            other.transform.Translate(transform.rotation * -Vector3.right*speed2);
        }
    }
}
