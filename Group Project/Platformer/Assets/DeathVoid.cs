﻿using UnityEngine;
using System.Collections;

public class DeathVoid : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}

    // Update is called once per frame
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            other.gameObject.transform.position = other.gameObject.GetComponent<PlayerController25d>().CheckPoint;
        }
        if (other.gameObject.tag == "Crate")
        {
            Destroy(other.gameObject);
        }
    }
}
