﻿using UnityEngine;
using System.Collections;

public class EntitySpawner : MonoBehaviour {
    public float Frequency = 2.0f;
    public GameObject ToSpawn;
	// Use this for initialization
	void Start () {
        StartCoroutine(Spawn());
	}
	
	IEnumerator Spawn () {
        yield return new WaitForSeconds(Frequency);
        Instantiate(ToSpawn, transform.position, ToSpawn.transform.rotation);
        StartCoroutine(Spawn());

    }
}
