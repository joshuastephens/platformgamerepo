﻿using UnityEngine;
using System.Collections;
[ExecuteInEditMode]
public class CameraOffset : MonoBehaviour
{

    [SerializeField] private Vector3 rot;
    [SerializeField] private GameObject objectToFollow;
    [SerializeField] private Vector3 offset;

    private Vector3 pRotation;
	// Use this for initialization
	void Start () {
        pRotation = objectToFollow.GetComponent<PlayerController25d>().currentRotation;
    }

    // Update is called once per frame
    void Update ()
	{
        pRotation = objectToFollow.GetComponent<PlayerController25d>().currentRotation;

        transform.rotation = Quaternion.Euler(rot+pRotation);


        transform.position = objectToFollow.transform.position + Quaternion.Euler(pRotation)*offset;
	}
}
