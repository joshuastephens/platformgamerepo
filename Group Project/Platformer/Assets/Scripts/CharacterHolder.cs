﻿using UnityEngine;

public class CharacterHolder : MonoBehaviour
{
    void OnTriggerStay(Collider collider)
    {
        collider.transform.rotation = transform.rotation;

        if (collider.transform.parent == null)
            collider.transform.parent = transform;

    }

    void OnTriggerExit(Collider collider)
    {
        if (collider.transform.parent == transform)
            collider.transform.parent = null;

        collider.transform.localScale = Vector3.one;
    }

}
