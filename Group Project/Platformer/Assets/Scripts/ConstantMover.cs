﻿using UnityEngine;
using System.Collections;

public class ConstantMover : MonoBehaviour
{

    public float speed = 1;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    transform.Translate(transform.localToWorldMatrix*transform.forward*speed);
	}
}
