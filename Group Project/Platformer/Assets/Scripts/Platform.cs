﻿using UnityEngine;
using System.Collections;

public class Platform : MonoBehaviour {

    void OnTriggerExit()
    {
        this.gameObject.GetComponent<BoxCollider>().isTrigger = false;
    }

    void OnCollisionExit()
    {
        this.gameObject.GetComponent<BoxCollider>().isTrigger = true;
    }
}
