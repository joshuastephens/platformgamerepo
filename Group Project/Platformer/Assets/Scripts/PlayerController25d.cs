﻿using System;
using UnityEngine;
using System.Collections;
using System.Net;
using System.Net.Sockets;

public class PlayerController25d : MonoBehaviour
{

    
    private Rigidbody rbody;
    [SerializeField]private GameObject weapon;
    [SerializeField]private float force = 100;
    [SerializeField]private float jumpForce = 20;
    [SerializeField]private Animation anim;
    [SerializeField]private string walkAnim = "Walking";
    [SerializeField]private string standAnim = "Standing";
    [SerializeField]
    private string attackAnim = "Attack";
    [SerializeField]
    private string attackAnim2 = "Attack2";
    [SerializeField]private float slerpBeta = .01f;
    
    public Vector3 newRotation = new Vector3(0,0,0);
    public Vector3 currentRotation = new Vector3(0, 0, 0);
    private Vector3 oldRotation = new Vector3(0, 0, 0);
    [SerializeField] private float rotBeta = .01f;
    private float rotTravelled = 1f;

    public Vector3 CheckPoint;

    private bool lookingRight = true;
    private float slerpTravelled = 1;
    private Vector3 Right = new Vector3(0, 120, 0);
    private Vector3 Left = new Vector3(0, 240, 0);
    private bool attacking = false;
    private bool canCombo = false;
    // Use this for initialization
    void Start()
    {
        rbody = this.GetComponent<Rigidbody>();
        CheckPoint = transform.position;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (canCombo)
        {
            if (anim.IsPlaying(attackAnim))
            {
                if (Input.GetKeyDown(KeyCode.LeftShift))
                {
                    anim.PlayQueued(attackAnim2);
                    attacking = true;
                    canCombo = false;
                }
            }
        }
        if (Input.GetKeyDown(KeyCode.LeftShift)&& !attacking)
        {
            weapon.SetActive(true);
            anim.Play(attackAnim);
            rbody.velocity=Vector3.zero;
            attacking = true;
            StartCoroutine(countSeconds());
        }
        if (!anim.IsPlaying(attackAnim)&&!anim.IsPlaying(attackAnim2))
        {
            canCombo = false;
            attacking = false;
            weapon.SetActive(false);
            Move();
        }
    }

    IEnumerator countSeconds()
    {
        yield return new WaitForSeconds(anim.GetClip(attackAnim).length*.3f);
        canCombo = true;
        
    }

    public void Move()
    {
        currentRotation = Quaternion.Slerp(Quaternion.Euler(oldRotation), Quaternion.Euler(newRotation), rotTravelled).eulerAngles;
      
            if (rotTravelled <1f)
            {
                rotTravelled += rotBeta;
            }
        


        transform.rotation = Quaternion.Slerp(Quaternion.Euler(currentRotation+Left), Quaternion.Euler(currentRotation + Right), slerpTravelled);
        if (lookingRight)
        {
            if (slerpTravelled <= 1)
            {
                slerpTravelled += slerpBeta;
            }
        }
        else
        {
            if (slerpTravelled >= 0)
            {
                slerpTravelled -= slerpBeta;
            }
        }

        if (Input.GetKey(KeyCode.D))
        {
            rbody.velocity = Quaternion.Euler(newRotation)*new Vector3(force, rbody.velocity.y, 0);
            anim.PlayQueued(walkAnim);
            lookingRight = true;
        }
        else if (Input.GetKey(KeyCode.A))
        {
            rbody.velocity = Quaternion.Euler(newRotation) * new Vector3(-force, rbody.velocity.y, 0);
            anim.PlayQueued(walkAnim);
            lookingRight = false;
        }
        else
        {
            if (!anim.IsPlaying(standAnim))
            {
                anim.CrossFade(standAnim);
            }
            rbody.velocity = new Vector3(0, rbody.velocity.y, 0);
        }
        

        if (Input.GetKeyDown(KeyCode.Space) &&rbody.velocity.y< .1f && rbody.velocity.y > -.1f)
        {
            rbody.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
        }
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            Destroy(this.gameObject);
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("EnemyWeakPoint"))
        {
            Destroy(other.gameObject.transform.parent.gameObject);
            rbody.velocity=new Vector3(rbody.velocity.x, 0,rbody.velocity.z);
            rbody.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
        }
    }


    public void setRotation(Vector3 newRot)
    {
        oldRotation = currentRotation;
        newRotation = newRot;
        rotTravelled = 0;
    }
}
