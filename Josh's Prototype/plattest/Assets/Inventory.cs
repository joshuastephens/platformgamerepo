﻿using System;
using UnityEngine;
using System.Collections.Generic;

public class Inventory : MonoBehaviour {

    private Dictionary<string,int> items = new Dictionary<string, int>();
    public List<string> itemstoshow=new List<string>(); 
    
    public void addItem(string item)
    {
        if (items.ContainsKey(item))
        {
            int size;
            items.TryGetValue(item, out size);
            items.Remove(item);
            items.Add(item,size+1);
            
        }
        else
        {
            items.Add(item, 1);
        }
        itemstoshow.Clear();
        foreach (var i in items)
        {
            itemstoshow.Add(i.Key+", "+i.Value);
        }
    }
}
