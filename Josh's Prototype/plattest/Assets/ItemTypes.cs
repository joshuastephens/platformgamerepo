﻿using UnityEngine;
using System.Collections;

public enum ItemTypes{
    Part,
    SpecialPart,
    Weapon,
    Armor,
}
