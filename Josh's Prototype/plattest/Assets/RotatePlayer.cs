﻿using UnityEngine;
using System.Collections;

public class RotatePlayer : MonoBehaviour
{
    [SerializeField] private Vector3 PlayerRotation1;
    [SerializeField]
    private Vector3 PlayerRotation2;

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            PlayerController25d p = other.gameObject.GetComponent<PlayerController25d>();
            if (p.newRotation != PlayerRotation1)
            {
                p.setRotation(PlayerRotation1);
            }
            else
            {
                p.setRotation(PlayerRotation2);
            }
        }
    }

    // Use this for initialization

}
