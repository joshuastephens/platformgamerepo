﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{

    private Rigidbody2D rbody;
    public float force = 100;
    public float jumpForce = 20;
	// Use this for initialization
	void Start ()
	{
	    rbody = this.GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
	    if (Input.GetKey(KeyCode.D))
	    {
	        rbody.velocity =new Vector2(force,rbody.velocity.y);
	    }
        else if (Input.GetKey(KeyCode.A))
        {
            rbody.velocity =new Vector2(-force,rbody.velocity.y);
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            rbody.AddForce(new Vector2(0,jumpForce),ForceMode2D.Impulse);
        }
    }
}
