﻿using System;
using UnityEngine;
public class item : MonoBehaviour
{
    [SerializeField]
    private ItemTypes type = ItemTypes.Part;
    [SerializeField]
    private string Name = "Default";

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            other.gameObject.GetComponent<Inventory>().addItem(Name);
            Destroy(this.gameObject);
        }
    }
}
