﻿using UnityEngine;
using System.Collections;

public class CameraFollowPlayer : MonoBehaviour {

    [SerializeField] private GameObject player;

    [SerializeField] private float offsetX = 0.0f;
    [SerializeField] private float offsetY = 2.0f;
    [SerializeField] private float offsetZ = -10.0f;

	void FixedUpdate () {
        // Keeps the camera behind the player
        Vector3 playerPosition = player.transform.position;
        this.gameObject.transform.position = new Vector3(playerPosition.x + offsetX, playerPosition.y + offsetY, playerPosition.z + offsetZ);
    }

}
