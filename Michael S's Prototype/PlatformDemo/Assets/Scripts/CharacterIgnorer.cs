﻿using UnityEngine;
using System.Collections;

public class CharacterIgnorer : MonoBehaviour
{
    private Collider parentCollider;

    void Start()
    {
        parentCollider = transform.parent.gameObject.GetComponent<BoxCollider>();
    }

    void OnTriggerEnter(Collider collider)
    {
        parentCollider.enabled = false;
    }

    void OnTriggerExit(Collider collider)
    {
        parentCollider.enabled = true;
    }
}
