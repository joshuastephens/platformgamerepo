﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class DemoController : MonoBehaviour
{
    [SerializeField]
    private float movementSpeed = 2.0f;
    [SerializeField]
    private float jumpSpeed = 2.0f;
    [SerializeField]
    private float gravityStrength = 1.0f;

    private float xAxisMovement;
    private float yAxisMovement;
    private float zAxisMovement;

    private const float speedModifier = 100.0f;

    Rigidbody playerRigidBody;

    void Awake()
    {
        playerRigidBody = GetComponent<Rigidbody>();
        playerRigidBody.freezeRotation = true;
    }

    void FixedUpdate()
    {
        xAxisMovement = Input.GetAxis("Horizontal") * speedModifier * movementSpeed * Time.deltaTime;
        zAxisMovement = Input.GetAxis("Vertical") * speedModifier * movementSpeed * Time.deltaTime;
        yAxisMovement = Input.GetAxis("Jump") * speedModifier * jumpSpeed * Time.deltaTime;

        if (yAxisMovement != 0)
            playerRigidBody.velocity = new Vector3(xAxisMovement, playerRigidBody.mass * yAxisMovement, zAxisMovement);
        else
            playerRigidBody.velocity = new Vector3(xAxisMovement, -gravityStrength * playerRigidBody.mass, zAxisMovement);

        // This creates a neat sliding effect!
        //Vector3 parentTransformPosition = gameObject.transform.parent.position;
        //rigidbody.velocity = new Vector3(parentTransformPosition.x + movex, 0.0f, parentTransformPosition.z + movez);

    }
}
