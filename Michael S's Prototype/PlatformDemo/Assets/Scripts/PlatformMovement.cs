﻿using UnityEngine;
using System.Collections.Generic;

public class PlatformMovement : MonoBehaviour
{
    [SerializeField]
    private Transform startPosition;
    [SerializeField]
    private Transform endPosition;

    //TODO Add waypoint movement
    [SerializeField]
    private List<Transform> positions;

    [SerializeField]
    private float movementSpeed = 1.0f;
    [SerializeField]
    private float resetTime = 1.0f;

    [SerializeField]
    private bool hasRotation = false;
    [SerializeField]
    private bool sphereicalInterpolation = false;

    private Transform newPosition;
    private bool goToEndPosition = false;

    void Start()
    {
        // If no starting position is given, default to the platform's start position
        if (startPosition == null)
        {
            startPosition = new GameObject().transform;
            startPosition.position = transform.position;
        }
        // If not end position is given, default to the platform's start position
        if (endPosition == null)
        {
            endPosition = new GameObject().transform;
            startPosition.position = transform.position;
        }

        ChangeTarget();
    }

    void FixedUpdate()
    {
        if (!sphereicalInterpolation)
        {
            if (hasRotation)
            {
                transform.position = Vector3.Lerp(transform.position, newPosition.position, movementSpeed * Time.deltaTime);
                transform.rotation = Quaternion.Lerp(transform.rotation, newPosition.rotation, movementSpeed * Time.deltaTime);
            }
            else
                transform.position = Vector3.Lerp(transform.position, newPosition.position, movementSpeed * Time.deltaTime);
        }
        else
        {
            if (hasRotation)
            {
                transform.position = Vector3.Slerp(transform.position, newPosition.position, movementSpeed * Time.deltaTime);
                transform.rotation = Quaternion.Slerp(transform.rotation, newPosition.rotation, movementSpeed * Time.deltaTime);
            }
            else
                transform.position = Vector3.Slerp(transform.position, newPosition.position, movementSpeed * Time.deltaTime);
        }
    }

    void ChangeTarget()
    {
        newPosition = goToEndPosition ? startPosition : endPosition;
        goToEndPosition = !goToEndPosition;
        Invoke("ChangeTarget", resetTime);
    }

    public void OnDrawGizmos()
    {

        if (hasRotation)
        {
            if (startPosition)
            {
                Gizmos.color = Color.green;
                Gizmos.DrawWireMesh(GetComponent<MeshFilter>().sharedMesh, endPosition.position, endPosition.rotation, transform.localScale);
            }
            if (endPosition)
            {
                Gizmos.color = Color.red;
                Gizmos.DrawWireMesh(GetComponent<MeshFilter>().sharedMesh, endPosition.position, endPosition.rotation, transform.localScale);
            }
        }
        else
        {
            if (startPosition)
            {
                Gizmos.color = Color.green;
                Gizmos.DrawWireCube(startPosition.position, transform.localScale);
            }
            if (endPosition)
            {
                Gizmos.color = Color.red;
                Gizmos.DrawWireCube(endPosition.position, transform.localScale);
            }
        }

    }

}
