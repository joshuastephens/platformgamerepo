﻿using UnityEngine;
using System.Collections;

public class PersonOnTop : MonoBehaviour {

    public GameObject levelPanel;

    void Start()
    {
        levelPanel.SetActive(false);
    }

	void OnTriggerEnter(Collider person)
    {
        levelPanel.SetActive(true);
    }

    void OnTriggerExit(Collider person)
    {
        levelPanel.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
