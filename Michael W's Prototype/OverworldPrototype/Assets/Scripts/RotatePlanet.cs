﻿using UnityEngine;
using System.Collections;

public class RotatePlanet : MonoBehaviour {
    private float moveSpeed = 20;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKey("w"))
        {
            this.transform.Rotate(-moveSpeed*Time.deltaTime, 0, 0, Space.World);
        }
        if (Input.GetKey("s"))
        {
            this.transform.Rotate(moveSpeed * Time.deltaTime, 0, 0, Space.World);
        }
        if (Input.GetKey("a"))
        {
            this.transform.Rotate(0, -moveSpeed * Time.deltaTime, 0, Space.World);
        }
        if (Input.GetKey("d"))
        {
            this.transform.Rotate(0, moveSpeed * Time.deltaTime, 0, Space.World);
        }
    }
}
