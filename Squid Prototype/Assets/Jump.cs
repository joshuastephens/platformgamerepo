﻿using UnityEngine;
using System.Collections;

public class Jump : MonoBehaviour
{
    
	// Update is called once per frame
	void Update () {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            Debug.Log("JUMP");
            this.gameObject.GetComponent<Rigidbody>().AddForce(transform.up * 500);
        }
        if(Input.GetKey(KeyCode.A))
        {
            this.gameObject.transform.Translate(-Vector3.right * 5.0f * Time.deltaTime);
        }
        if(Input.GetKey(KeyCode.D))
        {
            this.gameObject.transform.Translate(Vector3.right * 5.0f * Time.deltaTime);
        }	
	}
}
