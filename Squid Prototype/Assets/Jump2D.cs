﻿using UnityEngine;
using System.Collections;

public class Jump2D : MonoBehaviour {

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Debug.Log("JUMP");
            this.gameObject.GetComponent<Rigidbody2D>().AddForce(transform.up * 500);
        }
    }
}
